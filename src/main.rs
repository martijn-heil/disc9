/*
 disc9: A 9P2000 server for writing and reading to Discord.
 Copyright (C) 2019  Martijn Heil

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

extern crate discord;
extern crate rpassword;
extern crate byteorder;
extern crate nine;

use std::net::TcpStream;
use std::net::SocketAddr;
use std::net::TcpListener;

use nine::de::*;
use nine::p2000::*;

use discord::Discord;

use byteorder::{ReadBytesExt, WriteBytesExt, BigEndian, LittleEndian};

/*
 * For documentation on 9P2000 refer to http://ericvh.github.io/9p-rfc/rfc9p2000.html
 */

fn main() {
  std::process::exit(match run_app() {
    Err(err) => {
      eprintln!("Error: {}", err);
      1
    }
    Ok(_) => 0
  })
}

// TODO better error handling everywhere
fn handle_client(client: (TcpStream, SocketAddr)) -> Result<(), Box<dyn std::error::Error>> {
  let stream = client.0;
  let saddr = client.1;
  println!("Client connected from {}", saddr);

  // Fist request from the client should be a Tversion
  let version_msglen: u32 = stream.read_u32::<LittleEndian>().unwrap();
  msglen -= 4;
  let msgtype: u8 = stream.read_8::<LittleEndian>().unwrap();
  if msgtype != Tversion::msg_type_id() {
    panic!("First request received by client from {} is not of type Tversion.", saddr);
  }


  loop {
    // 9P uses little endian as the network byte order.. hurray for sane design!
    let msglen: u32 = stream.read_u32::<LittleEndian>().unwrap();
    msglen -= 4; // The sent prefixed message length includes the 4 byte long u32 length prefix.
    let msgtype: u8 = stream.read_u8::<LittleEndian>().unwrap(); // Endianness of course doesn't matter for a u8, but this is nice anyway.

    // T-messages are serverbound requests and R-messages are clientbound responses.
    match msgtype {
      t if Tattach::msg_tyoe_id() == t => {

      }

      t if Tauth::msg_type_id() == t => {

      }

      t if Tclunk::msg_type_id() == t => {

      }

      t if Tcreate::msg_type_id() == t => {

      }

      t if Tflush::msg_type_id() == t => {

      }

      t if Topen::msg_type_id() == t => {

      }

      t if Tread::msg_type_id() == t => {

      }

      t if Tremove::msg_type_id() == t {

      }

      t if Tstat::msg_type_id() == t {

      }

      t if Tversion::msg_type_id() == t {

      }

      t if Twalk::msg_type_id() == t {

      }

      t if Twrite::msg_type_id() == t {

      }

      t if Twstat::msg_type_id() == t {

      }
    }
  }
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
  let port = 9284; // randomly chosen by smashing on keyboard

  let mut stdin = std::io::stdin()
  let mut lines = stdin.lock().lines();

  // TODO consider storing sensitive information in a safer way, though this is not necessarily dangerous as it is.
  print!("Enter discord email address: ");
  let email = lines.next()ok_or(Err("Could not read discord email address."))?;

  print!("Enter discord password: ");
  let pass = lines.next().ok_or(Err("Could not read discord password."))?;

  let disc = Discord::new(email, pass).unwrap();

  let listener = TcpListener::bind(fmt!("127.0.0.1:{}", &port))?;
  println!("9P2000 server listening on 127.0.0.1:{}", &port);

  // Accept new connections
  for client in listener.incoming() {
    thread::spawn(move || {
      handle_client(client.unwrap());
    });
  }
}
